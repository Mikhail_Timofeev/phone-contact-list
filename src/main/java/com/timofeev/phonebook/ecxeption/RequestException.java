package com.timofeev.phonebook.ecxeption;

public class RequestException extends RuntimeException {
    public RequestException() {
    }

    public RequestException(String message) {
        super(message);
    }

    public RequestException(Throwable throwable) {
        super(throwable);
    }

    public RequestException(Throwable throwable, String message) {
        super(message, throwable);
    }
}
