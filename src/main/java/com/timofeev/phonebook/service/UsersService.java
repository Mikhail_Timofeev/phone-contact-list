package com.timofeev.phonebook.service;

import com.timofeev.phonebook.model.Contact;
import com.timofeev.phonebook.model.User;
import org.springframework.data.jpa.domain.Specification;

import java.util.List;

public interface UsersService {
    public List allUsers(Specification<User> userSpecification);

    public void saveUser(User user);

    public User getUser(Long id);

    public void updateUser(Long id, User user);

    public void deleteUser(Long id);

    public List<Contact> getContactList(Long id);

    public Specification<User> filterByFirstName(String firstName);

    public Specification<User> filterByLastName(String lastName);
}
