package com.timofeev.phonebook.service;

import com.timofeev.phonebook.ecxeption.RequestException;
import com.timofeev.phonebook.model.Contact;
import com.timofeev.phonebook.model.User;
import com.timofeev.phonebook.repo.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UsersServiceImpl implements UsersService {

    private UserRepo userRepo;

    @Autowired
    public void setUserRepo(UserRepo userRepo) {
        this.userRepo = userRepo;
    }

    @Override
    public List<User> allUsers(Specification<User> userSpecification) {
        return userRepo.findAll(userSpecification);
    }

    @Override
    public void saveUser(User user) {
        userRepo.save(user);
    }

    @Override
    public User getUser(Long id) {
        if (userRepo.existsById(id)) {
            return userRepo.findById(id).get();
        } else {
            throw new RequestException("Неверный ID");
        }
    }

    @Override
    public void updateUser(Long id, User user) {
        if (userRepo.existsById(id)) {
            userRepo.save(user);
        } else {
            throw new RequestException("Неверный ID");
        }
    }

    @Override
    public void deleteUser(Long id) {
        if (userRepo.existsById(id)) {
            userRepo.deleteById(id);
        } else {
            throw new RequestException("Неверный ID");
        }
    }

    @Override
    public List<Contact> getContactList(Long id) {
        return getUser(id).getContactList();
    }

    @Override
    public Specification<User> filterByFirstName(String firstName) {
        return (root, query, cb) -> firstName == null ? null : cb.like(root.get("firstName"), "%" + firstName + "%");
    }

    @Override
    public Specification<User> filterByLastName(String lastName) {
        return (root, query, cb) -> lastName == null ? null : cb.like(root.get("lastName"), "%" + lastName + "%");
    }
}
