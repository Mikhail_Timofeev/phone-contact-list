package com.timofeev.phonebook.service;

import com.timofeev.phonebook.ecxeption.RequestException;
import com.timofeev.phonebook.ecxeption.BadRequestException;
import com.timofeev.phonebook.model.Contact;
import com.timofeev.phonebook.repo.ContactRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.management.BadAttributeValueExpException;
import java.util.List;

@Service
public class ContactsServiceImpl implements ContactsService {

    private ContactRepo contactRepo;

    @Autowired
    public void setContactRepo(ContactRepo contactRepo) {
        this.contactRepo = contactRepo;
    }

    @Override
    public List allContacts(Specification<Contact> contactSpecification) {
        return contactRepo.findAll(contactSpecification);
    }

    @Override
    public void saveContact(Contact contact) {
        contactRepo.save(contact);
    }

    @Override
    public Contact getContact(Long id) {
        if (contactRepo.existsById(id)) {
            return contactRepo.findById(id).get();
        } else {
            throw new RequestException("Неверный ID");
        }
    }

    @Override
    public void updateContact(Long id, Contact contact) {
        if (contactRepo.existsById(id)) {
            contactRepo.save(contact);
        } else {
            throw new RequestException("Неверный ID");
        }
    }

    @Override
    public void deleteContact(Long id) {
        if (contactRepo.existsById(id)) {
            contactRepo.deleteById(id);
        } else {
            throw new RequestException("Неверный ID");
        }
    }

    @Override
    public Specification<Contact> filterByPhoneNumber(String phoneNumber) {
        return (root, query, cb) -> phoneNumber == null ? null : cb.like(root.get("phoneNumber"), "%" + phoneNumber + "%");
    }

    @Override
    public Long checkId(Long id) {
        if (id == null || id == 0) {
            throw new BadRequestException("Некорректный ID");
        } else {
            return id;
        }
    }
}
