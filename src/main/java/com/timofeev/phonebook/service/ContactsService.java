package com.timofeev.phonebook.service;

import com.timofeev.phonebook.model.Contact;
import org.springframework.data.jpa.domain.Specification;

import java.util.List;

public interface ContactsService {
    public List allContacts(Specification<Contact> contactSpecification);

    public void saveContact(Contact contact);

    public Contact getContact(Long id);

    public void updateContact(Long id, Contact contact);

    public void deleteContact(Long id);

    public Specification<Contact> filterByPhoneNumber(String phoneNumber);

    public Long checkId(Long id);


}
