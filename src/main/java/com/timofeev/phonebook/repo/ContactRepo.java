package com.timofeev.phonebook.repo;

import com.timofeev.phonebook.model.Contact;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ContactRepo extends JpaRepository<Contact, Long> {
    List<Contact> findAll(Specification<Contact> contactSpecification);
}
