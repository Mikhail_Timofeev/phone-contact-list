package com.timofeev.phonebook.controller;

import com.timofeev.phonebook.model.Contact;
import com.timofeev.phonebook.service.ContactsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/contacts")
public class ContactsController {

    private ContactsService contactsService;

    @Autowired
    public void setContactsService(ContactsService contactsService) {
        this.contactsService = contactsService;
    }

    @GetMapping()
    public List<Contact> getContacts(@RequestParam(value = "phone_number", required = false) String phoneNumber) {
        return contactsService.allContacts(Specification.where(contactsService.filterByPhoneNumber(phoneNumber)));
    }

    @GetMapping("/{id}")
    public Contact getContact(@PathVariable(value = "id") Long id) {
        Long checkId = contactsService.checkId(id);
        return contactsService.getContact(checkId);
    }

    @PostMapping()
    public void createContact(@RequestBody Contact contact) {
        contactsService.saveContact(contact);
    }

    @PutMapping("/{id}")
    public void updateContact(@PathVariable(value = "id") Long id, @RequestBody Contact contact) {
        Long checkId = contactsService.checkId(id);
        contactsService.updateContact(checkId, contact);
    }

    @DeleteMapping("/{id}")
    public void deleteContact(@PathVariable(value = "id") Long id) {
        Long checkId = contactsService.checkId(id);
        contactsService.deleteContact(checkId);
    }
}
