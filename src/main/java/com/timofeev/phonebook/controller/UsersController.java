package com.timofeev.phonebook.controller;

import com.timofeev.phonebook.model.Contact;
import com.timofeev.phonebook.model.User;
import com.timofeev.phonebook.service.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/users")
public class UsersController {

    private UsersService usersService;

    @Autowired
    public void setUsersService(UsersService ucs) {
        this.usersService = ucs;
    }

    @GetMapping()
    public List<User> getUsers(@RequestParam(value = "first_name", required = false) String firstName,
                               @RequestParam(value = "last_name", required = false) String lastName) {
        return usersService.allUsers(Specification.where(usersService.filterByFirstName(firstName).and(usersService.filterByLastName(lastName))));
    }

    @GetMapping("/{id}")
    public User getUser(@PathVariable(value = "id") Long id) {
        return usersService.getUser(id);
    }

    @PostMapping()
    public void createUser(@RequestBody User user) {
        usersService.saveUser(user);
    }

    @PutMapping("/{id}")
    public void updateUser(@PathVariable(value = "id") Long id, @RequestBody User user) {
        usersService.updateUser(id, user);
    }

    @DeleteMapping("/{id}")
    public void deleteUser(@PathVariable Long id) {
        usersService.deleteUser(id);
    }

    @GetMapping("/{id}/list")
    public List<Contact> getContactList(@PathVariable Long id) {
        return usersService.getContactList(id);
    }
}
