package com.timofeev.phonebook;

import com.timofeev.phonebook.controller.ContactsController;
import com.timofeev.phonebook.ecxeption.BadRequestException;
import com.timofeev.phonebook.model.Contact;
import com.timofeev.phonebook.service.ContactsService;
import com.timofeev.phonebook.service.ContactsServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.util.ArrayList;
import java.util.List;

@RunWith(SpringRunner.class)
@WebMvcTest(ContactsController.class)
public class ContactsControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ContactsService contactsService;

    @Test
    public void getContacts() throws Exception {
        List<Contact> contactList = new ArrayList<>();
        contactList.add(new Contact(1, "800", 1L));
        contactList.add(new Contact(2, "808", 1L));
        contactList.add(new Contact(3, "700", 1L));
        contactList.add(new Contact(4, "555", 1L));

        Mockito.when(contactsService.allContacts(Mockito.any())).thenReturn(contactList);

        mockMvc.perform(get("/app/contacts")).andExpect(status().isOk());

    }

    @Test
    public void getContact() throws Exception {
        Contact contact = new Contact(1, "800", 1L);

        Mockito.when(contactsService.getContact(Mockito.any())).thenReturn(contact);

        mockMvc.perform(get("/app/contacts/1")).andExpect(status().isOk());
    }

    @Test
    public void getContactByIdZero() throws Exception {
        Mockito.when(contactsService.getContact(Mockito.any())).thenThrow(new BadRequestException());

        mockMvc.perform(get("/app/contacts/0")).andExpect(status().isBadRequest());
    }

    @Test
    public void getContactByIdNotNumber() throws Exception {
        Mockito.when(contactsService.getContact(Mockito.any())).thenReturn(null);

        mockMvc.perform(get("/app/contacts/ttt")).andExpect(status().isBadRequest());
    }

    @Test
    public void createContact() {
        Contact contact = new Contact(1, "800", 1L);
    }

    @Test
    public void updateContact() {
        Contact contact = new Contact(1, "800", 1L);

    }
}
